build_example:
	go build -o example example.go

print_table:
	./example ShowTable --headers 1,2,3,4,5,6,Hello --row-count=3

run_bars:
	./example runBars --count=3 --value=10000000 --refresh-rate=1

lint:
	golangci-lint run -c .golangci.yml
