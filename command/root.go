package command

import (
	"log"
	"os"

	"github.com/spf13/cobra"
)

// RootCmd usually used to print help, so don't have to pass anon function
var RootCmd = &cobra.Command{
	Use:   "example util",
	Short: "this is example of util with console gui",
}

func InitCommands() {
	initTableCommand()
	initProgressBarCommand()
}

func Execute() {
	InitCommands()
	if err := RootCmd.Execute(); err != nil {
		log.Println(err)
		os.Exit(1)
	}
}
