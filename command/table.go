package command

import (
	"fmt"
	"log"
	"math/rand"
	"os"

	"github.com/olekukonko/tablewriter"
	"github.com/spf13/cobra"
)

var (
	tableRowCount    = 0
	tableHeaderCount = 0
	tableHeaders     []string
)

func initTableCommand() {
	ShowTable.Flags().IntVarP(&tableRowCount, "row-count", "r", 0, "count of rows")

	if err := ShowTable.MarkFlagRequired("row-count"); err != nil {
		log.Fatalln(err)
	}

	ShowTable.Flags().IntVarP(&tableHeaderCount, "col-count", "c", 0, "count of table headers")
	ShowTable.Flags().StringSliceVar(&tableHeaders, "headers", []string{}, "header list")
	RootCmd.AddCommand(ShowTable)
}

var ShowTable = &cobra.Command{
	Use:   "ShowTable",
	Short: "show table with randomly generated values",
	Run: func(cmd *cobra.Command, args []string) {
		var Headers []string

		if len(tableHeaders) != 0 {
			Headers = tableHeaders
		} else {
			Headers = generateTableHeaders(tableHeaderCount)
		}

		printTable(genTableRows(tableRowCount, len(tableHeaders)),
			Headers)
	},
}

func printTable(data [][]string, header []string) {
	table := tablewriter.NewWriter(os.Stdout)

	table.SetHeader(header)

	table.SetRowLine(true)

	for _, v := range data {
		table.Append(v)
	}

	table.Render()
}

func generateTableHeaders(headerCount int) []string {
	headers := make([]string, 0)

	for i := 0; i < headerCount; i++ {
		header := "header_" + fmt.Sprintf("%d", i)
		headers = append(headers, header)
	}

	return headers
}

func genRandomString(n int) string {
	var letters = []rune("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789")

	s := make([]rune, n)
	for i := range s {
		s[i] = letters[rand.Intn(len(letters))]
	}

	return string(s)
}

func genTableRow(colsCount int) []string {
	row := make([]string, 0)
	for i := 0; i < colsCount; i++ {
		row = append(row, genRandomString(rand.Intn(4)+4))
	}

	return row
}

func genTableRows(rowsCount int, colsCount int) [][]string {
	tableRows := make([][]string, 0)
	for i := 0; i < rowsCount; i++ {
		tableRows = append(tableRows, genTableRow(colsCount))
	}

	return tableRows
}
