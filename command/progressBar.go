package command

import (
	"fmt"
	"math/rand"
	"sync"
	"time"

	"github.com/spf13/cobra"
	"github.com/vbauerster/mpb/v7"
	"github.com/vbauerster/mpb/v7/decor"
)

func initProgressBarCommand() {
	progressBarsCmd.Flags().IntVar(&progressBarMaxValue, "value", 10, "max value of progress val")
	progressBarsCmd.Flags().IntVar(&progressBarCount, "count", 1, "count of progress bars")
	progressBarsCmd.Flags().IntVar(&progressBarRefreshRate, "refresh-rate", 1, "refresh rate of progress bars")

	RootCmd.AddCommand(progressBarsCmd)
}

var (
	progressBarMaxValue    = 0
	progressBarCount       = 0
	progressBarRefreshRate = 1
)

var progressBarsCmd = &cobra.Command{
	Use:   "runBars",
	Short: "run progress bars",
	Run: func(cmd *cobra.Command, args []string) {
		runProgressBars(progressBarMaxValue, progressBarCount)
	},
}

func runProgressBars(totalCount int, barCount int) {
	var wg2 sync.WaitGroup

	p := mpb.New(mpb.WithWaitGroup(&wg2), mpb.WithRefreshRate(time.Second*time.Duration(progressBarRefreshRate)))
	wg2.Add(barCount)

	for i := 0; i < barCount; i++ {
		bar := p.AddBar(int64(totalCount),
			mpb.PrependDecorators(
				decor.Name("bar №"+fmt.Sprintf("%d", i+1)),
				decor.CountersNoUnit(" progress: %d / %d, ", decor.WCSyncWidth),
			),

			mpb.AppendDecorators(
				decor.Percentage(decor.WCSyncSpace),
				decor.Any(func(statistics decor.Statistics) string {
					return " | random string: " + genRandomString(rand.Int()%3+4)
				}),
			),
		)

		go func() {
			defer wg2.Done()

			for !bar.Completed() {
				start := time.Now()

				bar.Increment()
				bar.DecoratorEwmaUpdate(time.Since(start))
			}
		}()
	}

	p.Wait()
}
